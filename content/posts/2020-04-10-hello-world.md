---
title: "Hello, world!"
slug: "hello-world"
date: 2020-04-10
draft: false
tags:
  - c++
---

Hello! More posts are in the works... stay tuned!

```c++
#include <iostream>

int main() {
  std::cout << "Hello, world!\n";
}
```
