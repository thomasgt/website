---
title: "\"Hello, {}!\\n\", \"world\""
slug: "hello-world-fmt"
date: 2022-01-13
draft: false
tags:
  - c++
---

The long awaited second post...

```c++
#include <format>
#include <iostream>

int main() {
  std::cout << std::format("Hello, {}!\n", "world");
}
```
