---
title: "Fixing a Lightroom Migration"
slug: "fixing-lightroom-migration"
date: 2023-04-24
draft: true
tags:
  - lightroom
  - photography
  - sqlite
  - python
---

Intro about photo management solutions
- Started with Apple Aperture (RIP)
- Migrated to Adobe Lightroom (before Creative Cloud)
- Moved my images to the cloud

About a year ago I felt the urge to take ownership of my data
- Moved images onto a large external SSD and migrated to Lightroom Classic
- Backed up external drive to my Synology NAS
- Backed up NAS to Backblaze

Then I purchased an M2 MacBook Air.

